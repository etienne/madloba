// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery.min
//= require jquery_ujs
//= require bootstrap-sprockets

//= require cocoon
//= require typeahead.bundle.min

//= require bootstrap-select.min
//= require ajax-bootstrap-select.min

// library for cookie management
//= require jquery.cookie
// data table plugin
//= require jquery.dataTables.min
// select or dropdown enhancer
//= require chosen.jquery.min
// library for making tables responsive
//= require responsive-tables
// autogrowing textarea plugin
//= require jquery.autogrow-textarea
// application script for Charisma theme
//= require charisma

//= require leaflet

//= require leaflet-sidebar.min
//= require leaflet-function-button
//= require leaflet.markercluster
//= require leaflet.awesome-markers
//= require bouncemarker

//= require bootstrap-notify
//= require bootstrap-switch.min
//= require bootstrap-tagsinput

//= require_tree .